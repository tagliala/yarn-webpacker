# Yarn Webpacker

Inspired by:

- [Rails Webpacker](https://github.com/rails/webpacker)
- [React Redux CRUD App](https://github.com/rajaraodv/react-redux-blog)

## Main purpose

This project aims to swtich from a react-based 5.1 Rails application to a
standalone node application in a straight way.

## Commands

* `yarn webpack`: The same as Rails webpacker's `bin/webpack`
* `yarn webpack-watcher`: The same as Rails webpacker's `bin/webpack-watcher`
* `yarn webpack-dev-server`: Starts the webpack development server
* `yarn lint:javascript`: Lints the javascript files. Pass `-- --fix` to automatically fix errors

# UNSTABLE! I WILL PUSH FORCE ON THE MASTER BRANCH!

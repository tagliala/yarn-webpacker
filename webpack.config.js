var path = require('path')

var nodeEnv    = process.env.NODE_ENV || 'development'
var configFile = path.resolve('config/webpack/' + nodeEnv + '.js')

module.exports = require(configFile)

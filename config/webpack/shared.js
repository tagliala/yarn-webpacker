// Note: You must restart bin/webpack-watcher for changes to take effect

var path    = require('path')
var webpack = require('webpack')
var glob    = require('glob')
var extname = require('path-complete-extname')

module.exports = {
  entry: glob.sync(path.join('app', 'javascript', 'packs', '*.js*')).reduce(
    function(map, entry) {
      var basename = path.basename(entry, extname(entry))
      map[basename] = path.resolve(entry)
      return map
    }, {
      'react_bundle': ['react', 'react-dom', 'redux', 'react-redux']
    }
  ),

  output: {
    filename: '[name].js',
    // wepack-dev-server requires an absolute pack here
    path: path.resolve('public', 'packs')
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            'react',
            [ 'latest', { 'es2015': { 'modules': false } } ]
          ],
          plugins: ['transform-object-rest-spread']
        }
      }
    ]
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin('react_bundle')
  ],

  resolve: {
    extensions: [ '.js' ],
    modules: [
      path.resolve('app/javascript'),
      path.resolve('node_modules')
    ]
  },

  resolveLoader: {
    modules: [ path.resolve('node_modules') ]
  }
}

import React, { PropTypes } from "react"

const Link = ({ active, children, onClick }) => {
  if (active) {
    return <span className="btn btn-default active">
      {children}
    </span>
  }

  return (
    <a href="#" className="btn btn-default"
      onClick={e => {
        e.preventDefault()
        onClick()
      }}
    >
      {children}
    </a>
  )
}

Link.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
}

export default Link
